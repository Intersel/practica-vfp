# Práctica / Evaluación para personal de nuevo ingreso #

## Objetivo de la práctica ##

Que el candidato demuestre sus conocimiento, habilidades y potencial dentro de lo que cabe en la práctica requerida, basándose como mínimo en los criterios de evaluación indicados según el puesto al que aspira.

## Criterios de evaluación ##

1. Funcionalidad
2. Creatividad
3. Patrones y técnicas de diseño
4. Validaciones del sistema
5. Diseño/estructura de Base de datos
6. Manejo y explotación de la base de datos
7. Organización

## Requerimientos de la práctica ##

Diseñar y desarrollar un sistema de consulta de llamadas efectuadas por líneas celulares.

El sistema cuenta con un catálogo de líneas celulares, las cuales están registradas en el archivo [Líneas celulares.csv](https://bitbucket.org/Intersel/practica-vfp/src/master/L%C3%ADneas%20celulares.csv)

- **MobileLineId:** Id único de registro, nunca se repite.
- **MobileLine:** Linea/Número celular
- **Description:** Descripción 

Cada una de estas líneas celulares ha efectuado llamadas a distintos destinos del país, el registro detallado de las llamadas efectuadas se encuentra en el archivo [Detalle de llamadas.csv](https://bitbucket.org/Intersel/practica-vfp/src/master/Detalle%20de%20llamadas.csv)

- **CallDetailId:** Id único de registro, nunca se repite.
- **MobileLine:** Línea celular que origina la llamada
- **CalledPartyNumber:** Número marcado
- **CalledPartyDescription:** Descripción del lugar a donde se llamó.
- **DateTime:** Fecha y hora de la llamada
- **Duration:** Duración en segundos de la llamada
- **TotalCost:** Costo de la llamada.

La relación entre el catálogo de líneas celulares y el detalle de llamadas se puede establecer a través de la columna **MobileLine** o el **Id único de registro** (requiere cambios).

Se deberá importar la información de estos archivos a tablas DBF’s y posteriormente escalar a tablas en una base de datos Microsoft SQL Server para ser consultadas por el sistema.

### Consulta de llamadas con filtros ###

En este formulario se capturan los siguientes filtros para la consulta:

- **Número celular:** Puede estar vacío, parcial o completo
- **Lugar a donde se llamó:** Puede estar vacío, parcial o completo
- **Rango de fecha y hora:** Rango de fecha y hora de llamada

![Formulario](https://bitbucket.org/Intersel/practica-vfp/raw/4c465976875447f10cb3f42f5b3cc3a055273200/Formulario.PNG) 

> *Formulario de ejemplo. Para fines de la práctica, proponga el diseño y componentes*

Al oprimir el botón <Consultar> se mostrará un reporte en Fox (Fox Reports), **las consultas deben ser sobre las tablas en la base de datos Sql Server**.

Los campos para mostrar en el reporte de llamadas son los siguientes: 

- Celular
- Descripción
- Número marcado
- Lugar
- Fecha y hora
- Duración
- Costo.

![Reporte](https://bitbucket.org/Intersel/practica-vfp/raw/4c465976875447f10cb3f42f5b3cc3a055273200/Reporte.PNG)

> *Formulario de ejemplo.  Para fines de la práctica, proponga el diseño y componentes*

### Implementación ###

Para la implementación del sistema puede utilizar las siguientes tecnologías:

- Visual Fox Pro, VFP Forms, VFP Reports, Microsoft SQL Server, Stored Procedures.
